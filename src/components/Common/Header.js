import React, {Fragment} from 'react'
import {observer, inject} from 'mobx-react'
import {Link, Route} from 'react-router-dom'

@inject('commonStore')
@observer
class NavLink extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        const {label, to} = this.props;
        const {get_current_url} = this.props.commonStore;

        return <Route
            path={to}
            exact={true}
            children={(props) => {
                const {match} = props;
                return (<li className={match ? "nav-item active" : "nav-item"}>
                    <Link className="nav-link" to={to}>{label}</Link>
                </li>)
            }}
        />
    }
}

@inject('commonStore', 'userStore')
@observer
export default class Header extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        const {userStore} = this.props;
        return (
            <section id="section-header">
                {/* Header */}
                <header>
                    <div className="menu-top">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 col-md-7">
                                    <ul className="menu-adddress ">
                                        <li>
                                            <a href="#">
                                                <span className="icons-add"/>
                                                <span>1703 CMT8, P.10, Q,10 TP.HCM</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="tell:+1 254-752-3993">
                                                <span className="icons-phone"/>
                                                <span className="hidemobile">+1 254-752-3993</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="mailto:luctt@gmail.com">
                                                <span className="icons-mail"/>
                                                <span className="hidemobile">luctt@gmail.com</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div className="col-12 col-md-5">
                                    <ul className="menu-login">
                                        <li>
                                            <Link to="/dang-tin-ban-nha-dat" title="Đăng bài">Đăng bài</Link>
                                        </li>
                                        <li/>
                                        {
                                            userStore.current_user === null
                                                ? <Fragment>
                                                    <li>
                                                        <Link to="/login">Đăng nhập</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="/register">Đăng ký</Link>
                                                    </li>
                                                </Fragment>
                                                : <li>
                                                    <a href="#">{userStore.current_user.name}</a>
                                                </li>
                                        }

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="menu-primary">
                        {/*Navbar*/}
                        <nav className="navbar navbar-expand-lg navbar-dark white-color">
                            <div className="container">
                                <div className="row w-100">
                                    <Link className="navbar-brand d-none d-md-none d-lg-block" to="/">
                                        <img src="/assets/img/logo.png"/>
                                    </Link>
                                    <button className="navbar-toggler toggle-mobile" type="button"
                                            data-toggle="collapse" data-target="#navbarSupportedContent"
                                            aria-controls="navbarSupportedContent" aria-expanded="false"
                                            aria-label="Toggle navigation">
                                        <span className="navbar-toggler-icon"/>
                                    </button>
                                    <button className="navbar-toggler toggle-mobile search-mobile" type="button">
                                        <span className="fa far fa-search"/>
                                    </button>
                                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                        <ul className="navbar-nav ml-auto navbar-menu-primary">
                                            <NavLink to="/" label="HOME"/>
                                            <NavLink to="/mua-ban/1/nha-dat-ban" label="Nhà Đất Bán"/>
                                            <NavLink to="/mua-ban/2/nha-dat-cho-thue" label="Nhà Đất Cho Thuê"/>
                                            <NavLink to="/mua-ban/3/nha-dat-can-mua" label="Nhà Đất Cần Mua"/>
                                            <NavLink to="/mua-ban/4/nha-dat-can-thue" label="Nhà Đất Cần Thuê"/>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </nav>
                        {/*/.Navbar*/}
                    </div>
                </header>
                {/* End - Header */}

            </section>
        )
    }
}