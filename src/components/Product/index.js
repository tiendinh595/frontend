import React from 'react'
import {NotificationContainer, NotificationManager} from 'react-notifications';
import {withScriptjs, withGoogleMap, GoogleMap, Marker} from "react-google-maps"
import 'Styles/upload.css'


export class UploadItem extends React.Component {
    state = {is_deleted: false};

    constructor(props) {
        super(props)
    }

    onDelete = () => {
        const {id} = this.props.image;
        this.props.onDelete(id);
        this.setState({
            is_deleted: true
        });
        NotificationManager.success('xóa file thành công');


        // ApiCaller.remove(`/files/${id}`)
        //     .then(res => {
        //         if (res.code === 200) {
        //             this.setState({
        //                 is_deleted: true
        //             });
        //             NotificationManager.success('xóa file thành công');
        //             this.props.onDelete(id)
        //         }
        //         else
        //             NotificationManager.errr(res.message)
        //     })
        //     .catch(err => NotificationManager.errr('Có lỗi xảy ra'))
    };

    render() {
        const {image} = this.props;

        if (this.state.is_deleted)
            return null;

        return (
            <div className="upload-item">
                <NotificationContainer/>
                <img src={image.full_url} className="img-thumbnail"/>
                <i className="fa fa-trash" onClick={this.onDelete}/>
            </div>
        )
    }
}

export class ListImageUpload extends React.PureComponent {

    constructor(props) {
        super(props)
    }

    render() {
        const {images} = this.props;

        return (
            <div className="upload_wrap">
                {
                    images.map(image => <UploadItem key={image.id} image={image} onDelete={this.props.onDelete}/>)
                }
            </div>
        )
    }
}

export const MyMapComponent = withScriptjs(withGoogleMap((props) => {
    return <GoogleMap
        defaultZoom={15}
        defaultCenter={{lat: props.lat, lng: props.lng}}
    >
        <Marker
            position={{lat: props.lat, lng: props.lng}}
            draggable={true}
            onDragEnd={props.onChangeLocation}
        />
    </GoogleMap>
}));
