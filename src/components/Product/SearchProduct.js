"use strict";

import React, {Fragment} from 'react'
import BaseComponent from 'Components/BaseComponent'
import WindowTitle from 'Components/Layout/WindowTitle'
import MasterLayout from 'Components/Layout/MasterLayout'
import * as ApiCaller from 'Utils/ApiCaller'
import {Redirect} from 'react-router-dom'
import LoadingSpinner from 'Components/Common/LoadingSpinner'
import {NotificationContainer, NotificationManager} from 'react-notifications';
import FormSearch from 'Components/Common/FormSearch'
import Breadcrumb from 'Components/Common/Breadcrumb'
import ProductList from 'Components/Product/ProductList'
import ReactPaginate from 'react-paginate';
import {inject, observer, withRouter} from 'mobx-react'
import {getParams} from 'Utils/Helper'
import LoadingData from 'Components/Common/LoadingData'


@inject('commonStore')
@observer
export default class SearchProduct extends BaseComponent {

    constructor(props) {
        super(props);
        this.url_api = '';
        this.state = {
            is_loading: true,
            is_load_products: true,
            category: null,
            products: [],
            meta_data: {}
        }
    }

    loadProducts = async (url) => {
        this.setState({
            is_loading: true,
        });
        setTimeout(()=>{
            ApiCaller.get(url)
                .then(res => {
                    this.setState({
                        products: res.data,
                        meta_data: res.metadata
                    })
                })
                .finally(() => this.setState({is_loading: false}))
        }, 300);

    };

    async componentWillMount() {
        this.onSearch();
    }

    componentWillReceiveProps(newProps) {
        this.onSearch();
    }

    handlePageClick = (data) => {
        let url = `${this.url_api}&page=${data.selected + 1}`;
        this.loadProducts(url);
    };

    getDataBreadcrumb = () => {
        let result = [];
        const {pathname, search} = window.location;
        result.push({
            link: `/${pathname}${search}`,
            label: 'Tìm Kiếm'
        });
        return result;
    };

    onSearch = (params = null) => {
        params = params === null ? getParams() : params;
        this.setState({
            is_loading: true,
            is_searching: false,
            ...params
        });

        let {area, price, product_type, city_id, district_id} = params;

        area = area.split('-');
        let area_search = '';
        if (area.length === 2) {
            if (area[1] === '<')
                area_search = `&area_to=${area[0]}`;
            else if (area[1] === '>')
                area_search = `&area_from=${area[0]}`;
            else
                area_search = `&area_from=${area[0]}&area_to=${area[1]}`;
        }

        price = price.split('-');
        let price_search = '';
        if (price.length === 2) {
            if (price[1] === '<')
                price_search = `&price_to=${price[0]}`;
            else if (price[1] === '>')
                price_search = `&price_from=${price[0]}`;
            else
                price_search = `&price_from=${price[0]}&price_to=${price[1]}`;
        }


        this.url_api = `/products/?product_type_master_id=${product_type}&city_id=${city_id}&district_id=${district_id}${area_search}${price_search}`;
        this.loadProducts(this.url_api);
    };

    render() {
        const {is_loading, products} = this.state;

        return (
            <MasterLayout>

                <NotificationContainer/>
                <WindowTitle title="Tìm Kiếm"/>
                <Breadcrumb links={this.getDataBreadcrumb()}/>
                <FormSearch is_redirect={false} onSearch={this.onSearch}/>
                <div className="nhadatban_content">
                    <div className="container">


                        {
                            (() => {
                                if (!is_loading) {
                                    return (
                                        <div className="row">
                                            <ProductList type="grid" products={products}/>
                                        </div>
                                    )
                                } else {
                                    return (
                                        <LoadingData />
                                    )
                                }
                            })()
                        }
                        {
                            (() => {
                                if (products.length !== 0) {
                                    return (
                                        <div className="row">
                                            <div className="pagination_cate text-center w-100">
                                                <nav aria-label="pagination ">
                                                    <ReactPaginate previousLabel={"«"}
                                                                   previousClassName="page-item"
                                                                   nextClassName="page-item"
                                                                   previousLinkClassName="page-link"
                                                                   nextLinkClassName="page-link"
                                                                   nextLabel={"»"}
                                                                   breakLabel={<a href="">...</a>}
                                                                   breakClassName={"break-me"}
                                                                   pageCount={this.state.meta_data.total_pages}
                                                                   marginPagesDisplayed={2}
                                                                   pageRangeDisplayed={5}
                                                                   onPageChange={this.handlePageClick}
                                                                   containerClassName={"pagination justify-content-center"}
                                                                   subContainerClassName={"pages pagination"}
                                                                   activeClassName={"active"}
                                                                   pageClassName="page-item"
                                                                   pageLinkClassName="page-link"
                                                    />
                                                </nav>
                                            </div>
                                        </div>
                                    )
                                }
                            })()
                        }

                    </div>
                </div>


            </MasterLayout>
        )
    }

}